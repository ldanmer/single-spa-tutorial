# Single SPA tutorial

A simple example of how to build micro frontend APP with the help of [Single-SPA](https://single-spa.js.org/) library.

## How to use it
1. Clone this project
2. `yarn install`
3. `yarn start`
4. open http://localhost:8080/ in a web browser