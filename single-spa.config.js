import {registerApplication, start} from "single-spa";

registerApplication(
    'home',
    () => import('./src/home/app'),
        location => location.pathname === '/' || location.pathname.startsWith('/home'));

registerApplication(
    'about',
    () => import('./src/about/app'),
    location => location.pathname.startsWith('/about'));

start();