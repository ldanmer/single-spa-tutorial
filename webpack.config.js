const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    mode: 'development',
    entry: {
        // Set the single-spa config as the project entry point
        'single-spa.config': './single-spa.config.js',
        'common-dependencies': [
            'react',
            'react-dom',
            'vue',
        ],
    },
    output: {
        publicPath: '/dist/',
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                // Webpack style loader added so we can use materialize
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.js$/,
                exclude: [path.resolve(__dirname, 'node_modules')],
                loader: 'babel-loader',
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ],
    },
    node: {
        fs: 'empty',
    },
    resolve: {
        modules: [path.resolve(__dirname, 'node_modules')],
        alias: {
            vue: 'vue/dist/vue.js'
        },
    },
    plugins: [
        // A webpack plugin to remove/clean the output folder before building
        new CleanWebpackPlugin(),
        new VueLoaderPlugin()
    ],
    devtool: 'source-map',
    externals: [],
    devServer: {
        historyApiFallback: true,
    },
};